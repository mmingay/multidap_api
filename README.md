# Multidap API
  
To use the API just append the `lib_info?` followed by the libname the following to the url.

For example:

`http://128.55.206.18/:60000/lib_info?libname=Control_TF_DNA_C_basilensis_4G11_DAPi709E12-ORGi5023-96P004`

or

`http://web.multidap-api.dev-cattle.stable.spin.nersc.org:60000/lib_info?libname=Control_TF_DNA_C_basilensis_4G11_DAPi709E12-ORGi5023-96P004`

more to come
