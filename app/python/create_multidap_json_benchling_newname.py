import json
import os
import pandas as pd
import datetime
import sys
import numpy as np
import subprocess
import requests
import sys
import shortuuid
import time
##########
incsv = sys.argv[1]
# incsv = "/Users/mmingay/Desktop/jgi/dap_tool_updates/clarity_csv_example_daptool.csv"
# outjson = sys.argv[2]

########

class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)
        
############

# protein_rename = {"Protein Plate": "plate-barcode",
#                       "TF DNA Sample Genus": "genus",
#                       "TF DNA Sample Species": "species",
#                       "TF DNA Sample Strain": "strain",
#                       "TF DNA Reference FASTA": "reference",
#                       "TF Expression Template Lot #": "lot-number",
#                       "Well Position": "well-location",
#                       "Plasmid": "Plasmid",
#                       "TF DNA": "TF DNA"
#                      }

protein_rename = {"Protein Plate": "plate-barcode",
                      "TF DNA Genus": "genus",
                      "TF DNA Species": "species",
                      "TF DNA Strain": "strain",
                      "TF DNA Reference FASTA": "reference",
                      "TF Expression Template Lot #": "lot-number",
                      "Well Position": "well-location",
                      "Plasmid": "Plasmid",
                      "TF DNA": "TF DNA"
                     }


cid_submitted_by = 21
pool_concentration = 18.1
pool_volume = 20
pool_template_size = 214
pool_molarity = 129.9
# dap_experiment_date = "2020-09-01"
number_of_pcr_cycles = 10

df = pd.read_csv(incsv)
dap_experiment_date = df['DAP-Seq Experiment Date'].values[0].split("T")[0]
dap_experiment_id = shortuuid.ShortUUID().random(length=15)

jsondir = "/Users/mmingay/testing/bitbucket/multidap_api/app/json_files/"
csvdir = "/Users/mmingay/testing/bitbucket/multidap_api/app/csv_files/"
# jsondir = "json_files/"
# csvdir = "csv_files/"

outcsv = csvdir + dap_experiment_id + "_benchling.csv"
outjson = jsondir + dap_experiment_id + "_multidapinfo.json"

os.system('cp ' + incsv + ' ' + outcsv)

tempdir = "/Users/mmingay/testing/bitbucket/multidap_api/app/tmp/"

######

# for padding wells
def pad_well(well):    
    letters = ''.join([s for s in well if not s.isdigit()])
    nums = ''.join([s for s in well if s.isdigit()])
    if len(nums) == 1:
        newnum = nums.zfill(2)
    else:
        newnum = nums
    wellid = ''.join([letters,newnum])
    return wellid

# get ref and anno jat keys from RQC References Repo

def get_refrep_keys(spid):
    api = "https://rqc.jgi-psf.org/api/seq_jat_import/spid_to_ref/" + str(spid)
    r = requests.get(api)
    jobj = r.json()  # jobj is the dict of the returned JSON data
    ############## should add a test to see if the original ref file were purged
    ref_file = None
    ref_jatid = None
    ann_file = None
    ann_jatid = None
    if jobj['Genome']:
        ref_file = jobj['Genome'][0]['file_path']
        ref_jatid = jobj['Genome'][0]['jat_key']
    else:
        if jobj['fetch']:
            print('please assign or restore the reference files for: ' + str(spid) + ' via https://rqc.jgi-psf.org/seq_jat_import/page/assign')
        else:
            print('Fatal error: please assign the reference file for: ' + str(spid) + ' via https://rqc.jgi-psf.org/seq_jat_import/page/assign')

    if jobj['Annotation']:
        ann_file = jobj['Annotation'][0]['file_path']
        ann_jatid = jobj['Annotation'][0]['jat_key']
    else:
        if jobj['fetch']:
            print('please assign or restore the reference files via https://rqc.jgi-psf.org/seq_jat_import/page/assign')
        else:
            print('Fatal error: please assign the annotation file via https://rqc.jgi-psf.org/seq_jat_import/page/assign')
    if ref_jatid and ann_jatid:
        return [ref_jatid, ann_jatid]
    else:
        return False

# get pool stats from user input

def create_dap_pool_dict(pool_concentration, pool_volume, pool_template_size, pool_molarity):
    odict = {'concentration-ng-ul': pool_concentration,
            'volume-ul': pool_volume,
            'avg-template-size-bp':  pool_template_size,
            'molarity-pm': pool_molarity}
    return odict

def run_cmd(cmd):
    cmd_return = subprocess.run(cmd, shell=True, stderr=subprocess.STDOUT, universal_newlines=True)
    time.sleep(10)
    return cmd_return.split("\n")

# Get all organism info for grabbing stats from
def get_organism_dict(df):
    # grab spids and use to extract info from csv
    orgdict = {}
    ospids = df[['Genus', 'Species', 'Strain', 'Sequencing Project ID']].drop_duplicates()['Sequencing Project ID'].values
    # grab columns from csv
    orgcols = ['Fragment Library Sample Lot #', 'Genus', 'Species', 'Strain', 'Sequencing Project ID', 'Number of PCR Cycles', 'Fragment Library Sample ID', "Sequencing Project Name"]
    # rename columns to match json
    rename_cols = {"Fragment Library Sample Lot #": "lot-number",
                  "Genus": "genus",
                  "Species": "species",
                  "Strain": "strain",
                  "Sequencing Project ID": "sequencing-project-id",
                  "Number of PCR Cycles": "amplification-cycles",
                  "Fragment Library Sample ID": "sample-id"
                 }
    dforg = pd.DataFrame()
    for spid in ospids:
        tempdict = {}
        dfsub = df[df['Sequencing Project ID'] == spid].iloc[0]
        tempdict['lot-number'] = dfsub['Fragment Library Sample Lot #']
        tempdict['genus'] = dfsub['Genus']
        tempdict['species'] = dfsub['Species']
        tempdict['strain'] = dfsub['Strain']
        tempdict['sequencing-project-id'] = dfsub['Sequencing Project ID']
        tempdict['amplification-cycles'] = dfsub['Number of PCR Cycles']
        tempdict['sample-id'] = dfsub['Fragment Library Sample ID']
        tempdict['sample-name'] = str(dfsub['Fragment Library Sample ID'])
        reflist = get_refrep_keys(dfsub['Sequencing Project ID'])
        if reflist:
            tempdict['reference'] = reflist[0]
            tempdict['annotation'] = reflist[1]
        else:
            print("please assign ref")
            sys.exit()
        tempdict['sequencing-project-name'] = dfsub['Sequencing Project Name']
#         indexname = str(dfsub['i7 Primer']) + "-" + str(dfsub['i5 Primer'])
        indexname = str(dfsub['i5 Primer'])
#         demultiname = str(dfsub['TF DNA'] + "_" + dfsub['Genus'] + "_" + dfsub['Species'] + "_" + dfsub['Strain'] + "_" + str(datetime.datetime.now()).replace(" ", "_")).replace(":", "_")
        #timestring = str(datetime.datetime.now()).replace(" ", "_").replace(":", "").replace("-", "").split(".")[0][:-2]
        demultiname = str(dfsub['Genus'][0] + "_" + dfsub['Species'] + "_" + dfsub['Strain'])# + "_" + timestring
        tempdict['index-name'] = indexname
        tempdict['demultiplexed-library-name'] = demultiname
        orgdict[spid] = tempdict
    return orgdict

# grab stats for a given SPID from the master organism dict created by previous function
def get_organism_indexes_dict(orgdict, spid, i7, dpre, plateid, subdictkeys=['sample-id', 'index-name', 'demultiplexed-library-name']):
    tdict = {k: orgdict[spid][k] for k in subdictkeys}
    i5p = tdict['index-name']
    newname = i7 + "-" + i5p
    tdict['index-name'] = newname
#     timestring = timestring =str(datetime.datetime.now()).replace(" ", "_").split(".")[0]
    demulti_new = str(dpre + "_" + tdict['demultiplexed-library-name']).replace(" ", "_") + "_" + newname + "-" + plateid
    tdict['demultiplexed-library-name'] = demulti_new
    return tdict

# Handle whether a sample comes from gDNA or 
def get_tf_synbio_source(df, plate, well, protein_rename=protein_rename):
    welldf = df[list(protein_rename.keys())]
    plasmid = welldf['Plasmid'].isnull().values[0]
    tfdna = welldf['TF DNA'].isnull().values[0]
    plasmidvalue = str(welldf['Plasmid'].values[0])
    if len(welldf['Well Position'].unique()) > 1:
        print("check get_tf_sourcetype() input")
    else:
        if plasmid:
            synbio = "None"
        else:
            synbio = plasmidvalue
        if tfdna:
            sourcetype = "Plasmid"
        else:
            sourcetype = "gDNA"
    return sourcetype, synbio

# Get family name based on synbiosource above
def get_family_name(df, plate, well, protein_rename=protein_rename):
    welldf = df[list(protein_rename.keys()) + ["TF DNA Family","Plasmid TF DNA Family", "TF DNA Common Name", "Plasmid TF DNA Common Name"]]
    sourcetype = get_tf_synbio_source(welldf, plate, well, protein_rename=protein_rename)[0]
    if sourcetype == "gDNA":
        fam = welldf['TF DNA Family'].values[0]
        nickname = welldf['TF DNA Common Name'].values[0]
    else:
        fam = welldf['Plasmid TF DNA Family'].values[0]
        nickname = welldf['Plasmid TF DNA Common Name'].values[0]
    if str(fam) == 'nan':
        fam = "None"
    if str(nickname) == 'nan':
        nickname = "NA"
    return fam, nickname

# Get and format TF Source Info
def get_tfdna_dict(df, plate, well, protein_rename=protein_rename):
    odict = {}
    sourcetype, synbio = get_tf_synbio_source(df, plate, well)
    odict['genus'] = df['TF DNA Genus'].values[0]
    if str(odict['genus']) == 'nan':
        odict['genus'] = "NA"
    odict['species'] = df['TF DNA Species'].values[0]
    if str(odict['species']) == 'nan':
        odict['species'] = "NA"
    odict['strain'] = df['TF DNA Strain'].values[0]
    if str(odict['strain']) == 'nan':
        odict['strain'] = "NA"
    ref = df['TF DNA Reference FASTA'].values[0]
    if str(ref) == 'nan':
        odict['reference'] = "None"
    else:
        odict['reference'] = df['TF DNA Reference FASTA'].values[0]
    odict['lot-number'] =  df['TF Expression Template Lot #'].values[0]
    odict['source-type'] = sourcetype
    odict['synbio-construct-name'] = synbio
    return odict

# Get and format Well Protein Info  
def get_well_info(df, plate, well, tfseq_dict):
    odict = {}
    fam, nickname = get_family_name(df, plate, well)
    odict['group-name'] = fam
    odict['protein-id'] = df['TF DNA'].values[0].replace(" ", "_")
    odict['dna-sequence'] = tfseq_dict[df["TF DNA ID"].values[0]]
    odict['well-location'] = pad_well(well)
    odict['nickname'] = nickname
    i7primer = df['i7 Primer'].values[0]
    dpre = df['TF DNA'].values[0]
    return odict, i7primer, dpre

# Make a dictionary with all TF sequences
def get_tf_sequences(sequences, outjson, apikey="sk_z3VXLTkxMxhBKfyzKVYOWet026jqa:sk_z3VXLTkxMxhBKfyzKVYOWet026jqa", url="curl https://jgi.benchling.com/api/v2/dna-sequences:bulk-get?dnaSequenceIds="):
    cmd = url  + ",".join(sequences) + " -u " + apikey + " > " + outjson
    os.system(cmd)
    with open(outjson, "r") as read_file:
        dnaseqs = json.load(read_file)
    try:
        tfdna_dict = {i['id']: i['bases'] if len(i['bases']) > 0 else "-" for i in dnaseqs['dnaSequences']}
    except:
        print(sequences)
    return tfdna_dict
#[unicode(x.strip()) if x is not None else '' for x in row]

# Create the master output JSON
def format_protein_plates(dfin):
    allseqs = list(set(dfin['TF DNA ID'].values))
    tempjson = tempdir + "temp_" + str(datetime.datetime.now()).replace(" ", "_").replace(":", "_").replace(".", "_") + ".json"
    tfseq_dict = get_tf_sequences(allseqs, tempjson)
    protein_plates = []
    orgdict = get_organism_dict(dfin)
    plates = dfin['Protein Plate'].unique()
    spids = dfin['Sequencing Project ID'].unique()
    for plate in plates:
        allwells = []
        outdict = {}
        condition1 = dfin['Protein Plate'] == plate
        tempdf = dfin[condition1]
        wells = [i for i in set(tempdf['Well Position'].values)]
        for well in wells:
            df = tempdf[tempdf['Well Position'].map(lambda x: x == well)]
            spids = df['Sequencing Project ID'].unique()
            wellinfo, i7, dpre = get_well_info(df, plate, well, tfseq_dict)
            tfdnasource = get_tfdna_dict(df, plate, well)
            orgindexes = [get_organism_indexes_dict(orgdict, i, i7, dpre, plate) for i in spids]
            welldict = {'TF-DNA-source': tfdnasource, 
                               'protein-id': wellinfo['protein-id'],
                               'dna-sequence': wellinfo['dna-sequence'],
                               'organism-indexes': orgindexes,
                               'group-name': wellinfo['group-name'],
                               'well-location': wellinfo['well-location'],
                               'nickname': wellinfo['nickname']
                              }
            allwells.append(welldict)
        outdict['plate-barcode'] = plate
        outdict['wells'] = allwells
        protein_plates.append(outdict)
    return protein_plates

# # Function to make keyfile for a link between CSV and JSON

# def get_all_libnames(injson, dap_experiment_id, keyfile="/Users/mmingay/testing/multidap_api/multidap_key_file.csv"):
#     all_libnames = []
#     with open(injson) as f:
#         data = json.load(f)
#         for i in range(len(data['protein-plates'])):
#             for x in range(len(data['protein-plates'][i]['wells'])):
#                 for y in range(len(data['protein-plates'][i]['wells'][x]['organism-indexes'])):
#                     all_libnames.append(data['protein-plates'][i]['wells'][x]['organism-indexes'][y]['demultiplexed-library-name'])
#     aldf = pd.DataFrame({'libname': all_libnames, 'id': dap_experiment_id})
#     if not os.path.isfile(keyfile):
#         aldf.to_csv(keyfile, header=True, index=False)
#     else: # else it exists so append without writing the header
#         df = pd.read_csv(keyfile)
#         mdf = pd.concat([aldf, df])
#         outdf = mdf.drop_duplicates(ignore_index=True)
#         outdf.to_csv(keyfile, header=False, index=False)

def get_all_libnames(injson, dap_experiment_id, keyfile="multidap_key_file.csv"):
    all_libnames, all_indexes = [], []
    with open(injson) as f:
        data = json.load(f)
        for i in range(len(data['protein-plates'])):
            for x in range(len(data['protein-plates'][i]['wells'])):
                for y in range(len(data['protein-plates'][i]['wells'][x]['organism-indexes'])):
                    all_libnames.append(data['protein-plates'][i]['wells'][x]['organism-indexes'][y]['demultiplexed-library-name'])
                    all_indexes.append(data['protein-plates'][i]['wells'][x]['organism-indexes'][y]['index-name'])
    aldf = pd.DataFrame({'libname': all_libnames, 'index-name': all_indexes, 'id': dap_experiment_id})
    if not os.path.isfile(keyfile):
        aldf.to_csv(keyfile, header=True, index=False)
    else: # else it exists so append without writing the header
        df = pd.read_csv(keyfile)
        mdf = pd.concat([aldf, df])
        outdf = mdf.drop_duplicates(ignore_index=True)
        outdf.to_csv(keyfile, header=False, index=False)
		

if __name__ == '__main__':
	mdict = {}
	# print("getting pool dict create_dap_pool_dict")
	mdict['dap-pool'] = create_dap_pool_dict(pool_concentration, pool_volume, pool_template_size, pool_molarity)
	mdict['submitted-by-cid'] = cid_submitted_by
	mdict['dap-experiment-date'] = dap_experiment_date
	mdict['number-pcr-cycles-dap-amplification'] = number_of_pcr_cycles
	# print("getting get_organism_dict")
	oodict=get_organism_dict(df).items()
	orglist = []
	# print("looping through orgnaism dict")
	for k,v in oodict:
		krm = ['index-name', 'demultiplexed-library-name']
		for key in krm:
			del v[key]
		orglist.append(v)
	mdict['organisms'] = orglist
	# print("formatting protein plates")
	mdict['protein-plates'] = format_protein_plates(df)
	# print("writing json")
	with open(outjson, 'w') as outfile:
		json.dump(mdict, outfile, indent=4, cls=NpEncoder)

	os.system("rm " + tempdir + "*")

	# print('creating keyfile')

	get_all_libnames(outjson, dap_experiment_id)

	print(str(outjson))