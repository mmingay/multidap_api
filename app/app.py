from flask import Flask
from flask_restful import Resource, Api, reqparse
import pandas as pd
import ast
import json


def get_plate_well(data, libname): 
    for i in range(len(data['protein-plates'])):
        for x in range(len(data['protein-plates'][i]['wells'])):
            for y in range(len(data['protein-plates'][i]['wells'][x]['organism-indexes'])):
                if data['protein-plates'][i]['wells'][x]['organism-indexes'][y]['demultiplexed-library-name'] == libname:
                    plate = str(i)
                    well=str(x)
                    orgind = str(y)
    return int(plate), int(well), int(orgind)

    
def get_organisms(injson, experiment_date):
    with open(injson) as f:
        data = json.load(f)
    mdict = {}
    mdict['jat-keys'] = {}
    orgnames, jatrefs, jatannos = [], [], []
    for i in range(len(data['organisms'])):
        organism_name = data['organisms'][i]['genus'] + "_" + data['organisms'][i]['species'] + "_" + data['organisms'][i]['strain']
        jatref, jatanno = data['organisms'][i]['reference'], data['organisms'][i]['annotation']
        jatrefs.append(jatref)
        jatannos.append(jatanno)
        orgnames.append(organism_name)
        mdict['jat-keys'][organism_name] = {'ref_jat_key': jatref, 'anno_jat_key': jatanno}
    mdict['all-organism-names'] = orgnames
    return mdict
    #return orgnames, jatrefs, jatannos
    
def get_all_libnames(injson, dap_experiment_id, keyfile="multidap_key_file.csv"):
    all_libnames, all_indexes = [], []
    with open(injson) as f:
        data = json.load(f)
        for i in range(len(data['protein-plates'])):
            for x in range(len(data['protein-plates'][i]['wells'])):
                for y in range(len(data['protein-plates'][i]['wells'][x]['organism-indexes'])):
                    all_libnames.append(data['protein-plates'][i]['wells'][x]['organism-indexes'][y]['demultiplexed-library-name'])
                    all_indexes.append(data['protein-plates'][i]['wells'][x]['organism-indexes'][y]['index-name'])
    aldf = pd.DataFrame({'libname': all_libnames, 'index-name': all_indexes, 'id': dap_experiment_id})
    if not os.path.isfile(keyfile):
        aldf.to_csv(keyfile, header=True, index=False)
    else: # else it exists so append without writing the header
        df = pd.read_csv(keyfile)
        mdf = pd.concat([aldf, df])
        outdf = mdf.drop_duplicates(ignore_index=True)
        outdf.to_csv(keyfile, header=False, index=False)
        
def get_dapseq_sampleid(incsv, indexname):
    csvdf = pd.read_csv(incsv)
    i7, i5 = indexname.split("-")[0], indexname.split("-")[1]
    dapseqid = csvdf[(csvdf['i7 Primer'] == i7) & (csvdf['i5 Primer'] == i5)]['DAP-Seq Sample'].values[0]
    return dapseqid

def get_filenames(libname, keyfile="multidap_key_file.csv"):
    mkdf = pd.read_csv(keyfile)
    uuid = mkdf[mkdf['libname'] == libname]['id'].values[0]
    csvfile = "csv_files/" + uuid + "_benchling.csv"
    jsonfile = "json_files/" + uuid + "_multidapinfo.json"
    return csvfile, jsonfile



def get_library_info(injson, incsv, libname):
    with open(injson) as f:
        data = json.load(f)
        plate, well, oindex = get_plate_well(data, libname)
        nickname = data['protein-plates'][plate]['wells'][well]['nickname']
        sampleid = data['protein-plates'][plate]['wells'][well]['organism-indexes'][oindex]['sample-id']
        morgindex = [i for i in range(len(data['organisms'])) if data['organisms'][i]['sample-id'] == sampleid][0]
        organism_name = data['organisms'][morgindex]['genus'] + "_" + data['organisms'][morgindex]['species'] + "_" + data['organisms'][morgindex]['strain']
        index_name = data['protein-plates'][plate]['wells'][well]['organism-indexes'][oindex]['index-name']
        experiment_date = data['dap-experiment-date']
        dapseq_id = get_dapseq_sampleid(incsv, index_name)
        organisms = get_organisms(injson, experiment_date)
        return {'libname': libname, 'tf-nickname': nickname, 'sample-id': sampleid, 'organism': organism_name, 'index': index_name, 'experiment-date': experiment_date, 'dap-seq-id': dapseq_id, 'all-organisms': organisms}
    
	
	
app = Flask(__name__)
api = Api(app)

class Dap(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('libname', required=True)
        args = parser.parse_args()
        libname = args['libname']
        csvfile, jsonfile = get_filenames(libname)
        data = get_library_info(jsonfile, csvfile, libname)
        return {'libinfo': data}, 200  # return data and 200 OK

api.add_resource(Dap, '/lib_info')  # add endpoint

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True) #app.run()  # run our Flask app
