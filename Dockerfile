FROM python:3.7

RUN mkdir -p app


RUN python -m pip install --upgrade pip

# We copy just the requirements.txt first to leverage Docker cache
COPY requirements.txt /app/requirements.txt



WORKDIR /app

RUN python -m pip install -r requirements.txt

COPY . /app

ENV USER mmingay
ENV UID 76137

RUN groupadd --gid 124 genome
RUN useradd -s /bin/false -u $UID -g genome $USER
USER $USER:genome

EXPOSE 5000

#CMD ["python","run.py"]

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]